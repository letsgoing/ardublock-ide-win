
/**
 *file:  sPSN_PongTester
 *author:  letsgoING -> info@letsgoing.de
 *
 *description:
 * Test-Software um Clients im Netzwerk "anzumelden"
 * Für das Netwerk werden min. 3 Arduinos mit IR-Link-Modulen benötigt:
 *
 * Arduino1: sPSN_Broker.ino
 * 
 * Arduino2: sPSN_PongTester.ino -> fragt Clients ab
 * 
 * Arduino3: sPSN_PingTest.ino -> reagiert auf PongTester
 * 
 *date:  01.12.2021
 */

#include "Arduino.h"

#include "SoftwareSerial.h"
#include "didacticNet.h"

//lege Geschwindigkeit für serielle Schnittstellen fest
#define SERIAL_BAUD 2400

//HIER CLIENT_ZAHL ANPASSEN
//*******************************
#define MAX_NR_CLIENTS 15
//*******************************

//lege Pins für SoftwareSerielle Schnittstelle fest
// Rx = 10 -> Empfänger | Tx = 11 -> Sender
SoftwareSerial sSerial(10, 11);

//Erzeuge Client-Instanz
DidacticPSNetClient psnClient;

//Arrays für Empfangs- und Sende Topic
char topicSub[MAX_LEN_TOPICS] = "";
char topicPub[MAX_LEN_TOPICS] = "";

char payloadSend[] = "Pong";

bool ledState = true;

void myCallback(char* mTopic, int mToLength, char* mData, int mDaLength) {
  Serial.println(mTopic);
  //reply pong
  strcpy(topicPub, mTopic);
  if (!strcmp(mData, "Ping")) {
    Serial.print(" ");
    Serial.println(mTopic);
    psnClient.publish(topicPub, payloadSend);
  }
}


void setup() {
  //Starte Serielle Schnittstelle (zum PC)
  Serial.begin(SERIAL_BAUD);
  delay(2000);

  Serial.print("\nSearching for ");
  Serial.print(MAX_NR_CLIENTS);
  Serial.println(" sPSNet-Clients");

  //Starte SoftwareSerielle Schnittstelle (zu IR-Link-Modulen)
  sSerial.begin(SERIAL_BAUD);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  psnClient.begin(sSerial, myCallback);

  //psnClient.subscribe("*");
  Serial.println("Topics: ");
  for (int clientNr = 0; clientNr < MAX_NR_CLIENTS; ++clientNr) {
    sprintf(topicSub, "%02d", clientNr);
    psnClient.subscribe(topicSub);
    while (!psnClient.handleNetwork());
    Serial.print(topicSub);
    Serial.print(" ");
  }

  Serial.println("\n\nReady for Clients...");

}

void loop() {
  //Verarbeiten der Daten, prüfen ob Netzwerk frei und versenden der Daten
  psnClient.handleNetwork();
}
