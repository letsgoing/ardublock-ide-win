# didacticNetwork

Bibliothek zur Implementierung von vereinfachten Netzwerken auf Arduino-Boards.
Die Funktionalitaeten entsprechen dem Stand der Technik, die Umsetzung ist didaktisch reduziert.

Für eine Nutzung der Bibliothek wird eine drahtlose Schnittstelle benoetigt, wleche sich an der Seriellen Schnittstelle betreiben lässt.
Wir empfehlen unsere IR-Link-Module, die Sie selbst herstellen koennen (Schaltplan und Layout bei uns anfragen - Kontakt über [letsgoING.org](httsp://letsgoING.org) 
oder z. B. hier erwerben koennen: [Hinweise zur Bestellung bei Aisler](#bestellung-bei-aisler).


## Download
Start download with the button on the right side. Choose the right format and start the download.

![](DownloadButton.png) 

After download install library:
https://www.arduino.cc/en/guide/libraries section "Importing a .zip Library"

# publish/subscribe network

## Beispiele

**Broker:**
- sPSN_Broker.ino        **->** Broker Applikation (muss nicht angepasst werden)

**Clients:**
- sPSN_Client1(2).ino    **->** Client Applikationen die sich gegenseitig einen analogen bzw. digtalen Wert senden
- sPSN-ClientMinimal.ino **->** Minimal-Code für den einfachen Einstieg
- sPSN-Chat.ino          **->** Chat-Applikation die Nachrichten unter eigenem Namen austauschen lässt

## Funktionen und Parameter 

### Client

```cpp
#Anlegen der Client-Instanz
// Anlegen des didacticPSNetClient-Objekts
// psnClient -> moeglicher Name für Objekt
didacticPSNetClient psnClient;

#Starten der Client-Instanz
void psnClient.begin(Stream& sSerial, fcn clientCallback);
// param1: Schnittstelle (Serial | SoftSerial)
// param2: Callback-Funktion (Bezeichnung frei waehlbar)

#Netzwerkverwaltung Client (Daten senden und Empfangen, Zugriffsregelung)
bool psnClient.handleNetwork();
// return: true wenn Daten versendet / false wenn nicht

void psnClient.setInterval(long intervalTime);
// param: Mindestwartezeit in ms zwischen zwei Sendevorgängen (default 500 ms)

#Topic eines anderen Clients abbonieren
int psnClient.subscribe(char* topic);
// param: Topic String/char-Array ("example" / char topic[n])
int psnClient.subscribe(char* topic, int length);
// param1: Topic String/char-Array
// param2: Anzahl Zeichen des Topics
// return ERROR-Wert: DN_ERROR_NO_ERROR, 
//                    DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten)

#Topic eines anderen Clients entfernen
bool psnClient.unsubscribe(char* topic);            
// param: Topic String/char-Array

bool psnClient.unsubscribe(char* topic, int length); 
// param1: Topic String/char-Array
// param2: Anzahl Zeichen des Topics
// return true wenn Daten versendet / false wenn nicht

#Daten unter Topic veroeffentlichen 
int psnClient.publish(char* topic, char* payload); 
// param1: Topic String/char-Array; 
// param2: payload-char-Array
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL, 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

int psnClient.publish(char* topic, bool payload); 
// param1: Topic String/char-Array; 
// param2: payload vom Datentyp bool (wird in char-Array gewandelt)
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL, 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

int psnClient.publish(char* topic, int payload); 
// param1: Topic String/char-Array; 
// param2: payload vom Datentyp int (wird in char-Array gewandelt)
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL, 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

int psnClient.publish(char* topic, int topicLength, char* payload, int payloadLength); 
// param1: Topic String/char-Array
// param2: Anzahl Zeichen des Topics
// param3: payload-char-Array
// param4: Anzahl Zeichen der Payload
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL, 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

//Daten werden beim Zustandswechsel von payload (0->1 / 1->0) veroeffentlicht
int psnClient.publishOnChange(char* topic, bool payload);
// param1: Topic String/char-Array; 
// param2: payload vom Datentyp bool (wird in char-Array gewandelt)
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL,
//                     DN_ERROR_NO_ERROR (Keine Daten veroeffentlicht und keine Fehler), 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

// Daten werden veroeffentlicht, wenn Veränderung von payload groeßer als threshold ist 
int psnClient.publishOnChange(char* topic, int payload, int threshold);
// param1: Topic String/char-Array; 
// param2: payload vom Datentyp bool (wird in char-Array gewandelt)
// param1: Topic String/char-Array; 
// return: ERROR-Wert: DN_PUBLISH_SUCCESSULL,
//                     DN_ERROR_NO_ERROR (Keine Daten veroeffentlicht und keine Fehler), 
//                     DN_ERROR_TOPIC_LEN (Topic zu lang - wird abgeschnitten), 
//                     DN_ERROR_PAYLOAD_LEN (Payload zu lange - wird abgeschnitten)

#Callback-Funktion (wird bei neuen Daten aufgerufen)
//Bezeichnung muss mit der in psnClient.begin(...) übereinstimmen
void clientCallback(char* topic, int topicLength, char* payload, int payloadLength){...}
// param1: Topic der Nachricht
// param2: Lange des Topics
// param3: Nutdaten der Nachricht
// param4: Laenge der Nachricht
```

### Broker

```cpp
#Anlegen der Broker-Instanz
// Anlegen des Broker-Objekts
// psnBroker -> moeglicher Name für Objekt
didacticPSNetBroker psnBroker; 

#Starten der Broker-Instanz 
void psnBroker.begin(Stream& sSerial); 
// param: Schnittstelle (Serial | SoftwareSerial)

#Netzwerkverwaltung Broker (Daten senden und Empfangen, Zugriffsregelung)
bool psnBroker.handleNetwork();
// return: true wenn Daten versendet / false wenn nicht

void psnBroker.setInterval(long intervalTime);
// param: Mindestwartezeit in ms zwischen zwei Sendevorgängen (default 0 ms)
```

### Hilfreiche Funktionen
Die Hiflsfunktionen sind als eigenständige Klassen implementiert und koennen so mehrfach (unter verschiedenen Namen) angelegt werden.
So koennen die Funktionen mehrfach, mit unterschiedlichen Parametern, verwendet werden.

```cpp
#Flankenerkennung z.B. fuer Taster
// Anlegen des EdgeDetector-Objekts
// eDetector -> moeglicher Name für Objekt
EdgeDetector eDetector;

int eDetector.edgeDetected(bool currentState);
// param: aktueller binärer Zustand
// return: 0 -> keine Flanke, RISING, FALLING

#Auf Wertaenderung groeßer Schwellwert pruefen
// Anlegen des ChangeDetector-Objekts
// cDetector -> moeglicher Name für Objekt
ChangeDetector cDetector

bool cDetector.valueChanged(int currentvalue, int threshold);
// param1: aktueller Wert
// param2: Schwellwert
// return: true -> Aenderung groeßer als Schwellwert; sonst false

#Nicht blockierendes Warten
// Anlegen des UnblockingTimer-Objekts
// uTimer -> moeglicher Name für Objekt
UnblockingTimer uTimer;

bool uTimer.timeElapsed(long);
// param: zu wartende Zeit in Millisekunden
// return: true, wenn Zeit Wartezeit abgelaufen; sonst false

#Einlesen von Text ueber Serialle Schnittstelle bis Endezeichen empfangen wird
// Anlegen des SerialReader-Objekts
// sReader -> moeglicher Name für Objekt
SerialReader sReader;

int sReader.readSerialData(Stream&, char*, char);
// param1: Schnittstelle (Serial | SoftSerial)
// param2: Array in das die Zeichen eingelesen werden
// param3: Endezeichen (char)
// return: Anzahl der eingelesenen Zeichen (0 wenn noch kein Endezeichen)
```

### Konstanten

Konstanten aus der Library die fuer die Programmierung genutzt werden und angepasst werden koennen.
```cpp
#ASCII Endezeichen
DN_ASCII_CR "carriage return CR" (int) 13
DN_ASCII_NL "new line NL"(int) 10

#Laenge Topics und Payload (Client und Broker)
MAX_LEN_TOPICS   default: 10
MAX_LEN_PAYLOAD  default: 20

#Anzahl Topics
MAX_NR_TOPICS_CLIENT  default: 5
MAX_NR_TOPICS_BROKER  default: 20

#Mindestwartezeiten zwischen Sendevorgängen (anpassen über psnClient.setInterval())
INTERVAL_CLIENT	 default: 500 ms
INTERVAL_BROKER	 default:   0 ms

#ERRORs (nicht verändern)
DN_PUBLISH_SUCCESSULL 1
DN_ERROR_NO_ERROR     0
DN_ERROR_TOPIC_LEN   -1
DN_ERROR_PAYLOAD_LEN -2

#Frame
MSG_PRELIMITER '<'
MSG_DELIMITER  '>'
MSG_SEPARATOR  '|'

#Dienste
MSG_PUBLISH     '@'
MSG_SUBSCRIBE   '?'
MSG_UPDATE      '#'
MSG_TOPIC_MULTI '*'
```

# Bestellung bei Aisler

- Account anlegen
- Link oeffnen: [Nachkaufprojekt bei Aisler.net](https://aisler.net/p/NBAQNHFV)
- Projekt importieren (Fehlermeldung / Hinweis ignorieren)
- Auf eigenen Account zurück gehen
- unter "Projekte -> Sandbox" befindet sich dann die Platine und die Bauteilliste
- vor der Bestellung Bauteilliste (Preise) aktualisieren
- Bestellung der Platinen (Peautiful Boards) und der Bauteile (Precious Parts)

**Hinweis:** Das Projekt letsgoING ist in **keiner Weise** an dem **Verkauf beteiligt**.