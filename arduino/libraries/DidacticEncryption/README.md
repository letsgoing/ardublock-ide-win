# DidacticEncryption

Bibliothek zur Implementierung von vereinfachten Verschlüsselungen auf Arduino-Boards.
Die Funktionalitaeten entsprechen dem Stand der Technik, die Umsetzung ist didaktisch reduziert.

## Download
Start download with the button on the right side. Choose the right format and start the download.

![](DownloadButton.png) 

After download install library:
https://www.arduino.cc/en/guide/libraries section "Importing a .zip Library"

# Symetrische Verschluesselung (XOR)
Diese KLasse ermöglicht eine einfache symmetrische Verschlüsselung
auf einem Arduino-Board mit 8-Bit-µController, wie z.B. dem Arduino Uno.
Der Algorithmus nutzt eine Exklusiv-Oder-Verknüpfung als Verschlüsselung.
Es wird immer ein Zeichen der Nutzdaten mit einem Zeichen des Schlüssels verknüpft.
Der Algortihmus ist für den ASCII-Zeichensatz geeignet.

- XOR Verknüpfung von Nachricht und Schlüssel
- "Endlos-Verknüpfung" (Datenlaenge != Schluessellaenge)
- Für 7-Bit ASCII-Code geeignet

## Beispiele
- symetric_minimal_.ino

## Funktionen und Parameter 

```cpp
#Anlegen der Instanz
// Anlegen des DidacticEncSymetric-Objekts
// symEncrypt -> moeglicher Name für Objekt
DidacticEncSymetric symEncrypt;

#Setzen des Schluessels für die symetrische Verschluesselung
int  symEncrypt.setKey(char*);
// param1: Schluessel-string (char-Array mit '\0' am Ende) mit der Laenge LENGHT_KEY (default 8) + 1
// return: ERROR-Wert: DE_ERROR_NO_ERROR, 
//                     DE_ERROR_KEYLENGHT_LONG  -> Key zu lang
//                     DE_ERROR_KEYLENGHT_SHORT -> Key zu kurz

#Symetrisch ver- und entschluesseln
void symEncrypt.crypt(char* messageIn, char* messageOut);
// param1: Eingehende Nachricht (Lesbarer Text oder verschluesselter Text)
// param2: Ausgehende Nachricht (Verschluesselter Text oder lesbarer Text)
//-> verwendet internen Key, welcher über setKey(char*) gesetzt wurde

void symEncrypt.crypt(char* messageIn, char* messageOut, char* key);
// param3: Key-string (char-Array mit '\0' am Ende)
// -> ignoriert internen Key - sinvoll wenn verschiedene Keys in einem Programm verwendet werden sollen.

void symEncrypt.crypt(char* messageIn,  int lenghtMessageIn, char* messageOut,  int lenghtMessageOur, char* key);
// param2 + param 4: Laenge der uebergebenen Nachrichten -> verwenden wenn kein ’\0' am Ende steht
```
### Konstanten
Konstanten aus der Library die fuer die Programmierung genutzt werden und angepasst werden koennen.

```cpp
#Laenge Key und Daten
DE_LENGHT_KEY	    default: 8
MAX_LEN_CYPHERTEXT	default: DE_LENGHT_KEY*4
MAX_LEN_PLAINTEXT	default: DE_LENGHT_KEY*4

#Errors (nicht veraendern!)
DE_ERROR_NOERROR          1
DE_ERROR_KEYLENGHT_LONG  -1
DE_ERROR_KEYLENGHT_SHORT -2
```

# Asymetrische Verschluesselung (angelehnt an RSA)

Diese Klasse ermöglicht eine asymmetrische Verschlüsselung
auf einem Arduino-Board mit 8-Bit-µController, wie z.B. dem Arduino Uno.
Der RSA-Algorithmus ist maximal reduziert umgesetzt.
Um die Laufzeit zu reduzieren, wird auf die Erzeugung von Primzahlen verzichtet.
Auch werden (für Verschlüsselungsalgorithmen) extrem kleine Primzahlen verwendet.
Weiter werden nur einzelne Bytes verschlüsselt und nicht die sonst übliche Kombination mehrerer.
So kann auf zusätzliche Bibliotheken für große Zahlen verzichtet werden.
Der Algortihmus ist (nur) für den ASCII-Zeichensatz geeignet.

- RSA ohne gängige zusätzliche Sicherheitsfeatures
- Keine Primzahlgenerierung
- Nur für Werte bis 127 geeignet -> 7-Bit ASCII-Code
- Verschlüsselte Daten werden als long-Zahlen in ASCII-Darstellung ausgegeben bzw. eingelesen 

## Beispiele
- asymetric_minimal_.ino

## Funktionen und Parameter 

```cpp
#Anlegen der Instanz
// Anlegen des DidacticEncAsymetric-Objekts
// asymEncrypt -> moeglicher Name für Objekt
DidacticEncAsymetric asymEncrypt;

#Schlüssel erzeugen und verwenden
void asymEncrypt.generateKeys();
//erzeugt Private-Key, Public-Key und RSA-Modul

long asymEncrypt.getPrivateKey();
// return: Private-Key

long asymEncrypt.getPublicKey();
// return: Private-Key

long asymEncrypt.getRsaModule();
// return: RSA-Modul (zur Verschluesselung in Kombination entweder mit Public- oder Private-Key)


#Asymetrisch verschluesseln
int asymEncrypt.encrypt(char* plainText, char* cypherText, long key, long rsaModule);
// param1: Eingehende Nachricht (Lesbarer Text)
// param2: Ausgehende Nachricht (Verschluesselter Text)
// param3: Key zum Verschluesseln der Nachricht
// param4: RSA-Modul zum Verschluesseln der Nachricht
// return: Lange der verschluesselten Nachricht (Anzahl Zeichen -> in ASCII-Zeichen gewandelte long-Werte )

int asymEncrypt.encrypt(char* plainText, int lengthOfPlainText, char* cypherText, long key, long rsaModule);
// param2: Laenge der eingehenden Nachricht -> verwenden wenn kein '\0' am Ende steht

#Asymetrisch entschluesseln
int asymEncrypt.decrypt(char* cypherText, char* plainText, long key, long rsaModule);
// param1: Eingehende Nachricht (verschluesselter Text)
// param2: Ausgehende Nachricht (lesbarer Text)
// param3: Key zum entschluesseln der Nachricht
// param4: RSA-Modul zum entschluesseln der Nachricht
// return: Laenge der entschluesselten Nachricht (Anzahl Zeichen)

void asymEncrypt.setRandomSeedPin(int);
// param1: unbenutzter Analog-Pin A0 bis A5 zur Erzeugung von (quasi-) zufaelligen Zahlen
```

### Konstanten

Konstanten aus der Library die fuer die Programmierung genutzt werden und angepasst werden koennen.
```cpp
#ASCII Trennzeichen zwischen Cypher-Werten
CYPHER_SEPARATOR   default:";"

#Maximale Laenge der verschluesselten Nachricht
MAX_LEN_CYPHERTEXT default: 250

#Primes 
MAX_NR_OF_PRIMES    48  (nicht veraendern)
MAX_PRIME_OFFSET    3
```
