/***************************************************************************************** 
 *  RSA-Testprogramm                                                                     *
 *  -----------------                                                                    *
 *  Dieses Beispielprogramm ermöglicht eine asymmetrische Verschlüsselung                *
 *  auf einem Arduino-Board mit 8-Bit-µController, wie z.B. dem Arduino Uno.             *
 *  Der RSA-Algorithmus ist maximal reduziert umgesetzt.                                 *
 *  Um die Laufzeit zu reduzieren, wird auf die Erzeugung von Primzahlen verzichtet.     *
 *  Auch werden (für Verschlüsselungsalgorithmen) extrem kleine Primzahlen verwendet.    *
 *  So kann auf zusätzliche Bibliotheken für große Zahlen verzichtet werden.             *
 *  Der Algortihmus für den ASCII-Zeichensatz geeignet.                                  *
 *                                                                                       *
 *  - RSA ohne gängige zusätzliche Sicherheitsfeatures                                   *
 *  - Keine Primzahlgenerierung                                                          *
 *  - Nur für Werte bis 127 geeignet -> 7-Bit ASCII-Code                                 *                                                 *
 *                                                                                       *                                     *
 *                                                                                       *
 *  Author: A. Buehler, T Moeck @ letsgoING                                              *
 *  Contact: info@letsgoing.de                                                           *
 *                                                                                       *
 *****************************************************************************************/

#include <DidacticEnc.h>

DidacticEncAsymetric asymEncrypt;

void setup() {
  Serial.begin(9600);
  delay(2000);
  char cypherText[MAX_LEN_CYPHERTEXT] = {0};
  char plainText[MAX_LEN_PLAINTEXT] = {0};

  long publicKey;
  long privateKey;
  long rsaModule;

  //zufaelliges Erzeugen der Keys
  asymEncrypt.generateKeys();

  publicKey = asymEncrypt.getPublicKey();
  privateKey = asymEncrypt.getPrivateKey();
  rsaModule = asymEncrypt.getRsaModule();

  Serial.print("public key:\t"); Serial.println(publicKey);
  Serial.print("private key:\t"); Serial.println(privateKey);
  Serial.print("rsa modul:\t"); Serial.println(rsaModule);
  
  char text[] = "Hallo letsgoING";
  Serial.print("Original:\t"); Serial.print(text);Serial.print("\tAnzahl Zeichen:\t");Serial.println(strlen(text));

  //Verchluesseln
  int lenghtCypherText = asymEncrypt.encrypt(text, cypherText, publicKey, rsaModule);
  Serial.print("Verschluesselt:\t"); Serial.print(cypherText);Serial.print("\tAnzahl Zeichen:\t");Serial.println(lenghtCypherText);

  //Entschluesseln
  int lenghtPlainText = asymEncrypt.decrypt(cypherText, plainText, privateKey, rsaModule);
  Serial.print("Entschlüsselt:\t"); Serial.print(plainText);Serial.print("\tAnzahl Zeichen:\t");Serial.println(lenghtPlainText);
}

void loop() {
  // put your main code here, to run repeatedly:
}
