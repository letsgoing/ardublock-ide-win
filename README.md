# letsgoING ArduBlock Arduino IDE - Windows
Um direkt mit dem Programmieren loslegen zu können, haben wir Dir für alle gängigen Betriebssysteme eine [Arduino IDE](https://www.arduino.cc/) vorbereitet.
- [Windows](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-win)
- [Mac](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-mac)
- [Linux](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-lin)

## Download
Über den Button auf der rechten Seite kannst Du den Download starten. Wähle einfach ein passendes Dateiformat und der Download startet von selbst.

![](DownloadButton.png) 

## Setup
1. Das .zip-File in ein lokales Verzeichnis entpacken
2. Arduino IDE starten

Vielen Dank für eure Unterstützung & viel Spaß mit dem neuen Simulator,

Euer letsgoING-Team 
